# O que é?
As portas TCP ("Transmission Control Protocol") são portas de protocolo responsável pela base de comucação de dados de toda a internet.

# Funções
- Garantir a entrega de datagramas IP.
- Executar a segmentação e o reagrupamento de grandes blocos de dados enviados pelos programas, garantir o sequenciamento adequado e a entrega ordenada de dados.
- Verificar a integridade dos dados transmitidos usando cãlculos de soma de verificação.
- Enviar mensagns positivas dependendo do recebimento bem-sucedido dos dados.
- Oferecer um método preferencial de transporte de progamas que devem usar transmissão confiável de dados baseada em sessões, como bancos de dados cliente/servidor e programas de correio eletrônico.

# Lista de portas mais usadas (IANA)
- 21  - FTP
- 23  - Telnet
- 25  - SMTP
- 80  - HTTP
- 110 - POP3
- 143 - IMAP
- 443 - HTTPS

# Lista completa de portas TCP e UDP
https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml

# Conceito de portas
Para uma compreensão mais fácil, usaremos o seguinte exemplo: suponha que, neste momento, você esteja usando um navegador de internet, um cliente de e-mail e um software de comunicação instantânea. Todas essas aplicações fazem uso da sua conexão à internet, mas como o computador faz para saber quais os dados que pertencem a cada programa? Simples, pelo número da porta que cada um utiliza. Por exemplo, se você está usando um programa de FTP (File Transfer Protocol), a conexão à internet é feita pela porta TCP 21, que é uma porta convencionada a este protocolo.

# Por que as portas mudaram?
Para cumprir as recomendações da Autoridade de Números Atribuídos à Internet (IANA), a Microsoft aumentou o intervalo dinâmico de porta do cliente para conexões de saída no Windows Vista e no Windows Server 2008. A nova porta inicial padrão é 49152 e a nova porta final padrão é 65535. Esta é uma alteração da configuração de versões anteriores do Windows que usavam um intervalo de porta padrão de 1025 a 5000.
    
